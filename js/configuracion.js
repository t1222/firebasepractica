
    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
    // TODO: Add SDKs for Firebase products that you want to use
    import {getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    const firebaseConfig = {
      apiKey: "AIzaSyCK4z7Oxop7PyIuCDfTOu46myCGIyK9TSY",
      authDomain: "ruyluna-523c6.firebaseapp.com",
      databaseURL: "https://ruyluna-523c6-default-rtdb.firebaseio.com",
      projectId: "ruyluna-523c6",
      storageBucket: "ruyluna-523c6.appspot.com",
      messagingSenderId: "896677866795",
      appId: "1:896677866795:web:f47ae32e1236b7cbc7e96e"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const db = getDatabase();


    var btnInsertar = document.getElementById("btnInsertar");
var btnBuscar = document.getElementById("btnBuscar");
var btnActualizar = document.getElementById("btnActualizar");
var btnBorrar = document.getElementById("btnBorrar");
var btnTodos = document.getElementById("btnTodos");
var lista = document.getElementById("lista");
var btnLimpiar = document.getElementById('btnLimpiar');
// Insertar
var matricula = "";
var nombre = "";
var carrera = "";
var genero = "";
function leerInputs(){
matricula = document.getElementById("matricula").value;
nombre = document.getElementById("nombre").value;
carrera = document.getElementById("carrera").value;
genero = document.getElementById('genero').value;
}
function insertDatos(){
leerInputs();
var genero= document.getElementById("genero").value;
set(ref(db,'alumnos/' + matricula),{
nombre: nombre,
carrera:carrera,
genero:genero})
.then((docRef) => {
alert("registro exitoso");
mostrarAlumnos();
console.log("datos" + matricula + nombre + carrera+ genero)
})
.catch((error) => {
alert("Error en el registro")
});
alert (" se agrego");
};
// mostrar datos
function mostrarAlumnos(){
const db = getDatabase();
const dbRef = ref(db, 'alumnos');
onValue(dbRef, (snapshot) => {
lista.innerHTML=""
snapshot.forEach((childSnapshot) => {
const childKey = childSnapshot.key;
const childData = childSnapshot.val();
lista.innerHTML = lista.innerHTML + "<div class='campo'> "  + childKey +
childData.nombre + childData.carrera +childData.genero +"<br> </div>";
console.log(childKey + ":");
console.log(childData.nombre)
// ...
});
}, {
onlyOnce: true
});
}
function actualizar(){
leerInputs();
update(ref(db,'alumnos/'+ matricula),{
nombre:nombre,
carrera:carrera,
genero :genero
}).then(()=>{
alert("se realizo actualizacion");
mostrarAlumnos();
})
.catch(()=>{
alert("causo Error " + error );
});
}
function escribirInpust(){
document.getElementById('matricula').value= matricula
document.getElementById('nombre').value= nombre;
document.getElementById('carrera').value= carrera;
document.getElementById('genero').value= genero;
}
function borrar(){
leerInputs();
remove(ref(db,'alumnos/'+ matricula)).then(()=>{
alert("se borro");
mostrarAlumnos();
})
.catch(()=>{
alert("causo Error " + error );
});
}
function mostrarDatos(){
leerInputs();
console.log("mostrar datos ");
const dbref = ref(db);
get(child(dbref,'alumnos/'+ matricula)).then((snapshot)=>{
if(snapshot.exists()){
nombre = snapshot.val().nombre;
carrera = snapshot.val().carrera;
genero = snapshot.val().genero;
console.log(genero);
escribirInpust();
}
else {
alert("No existe");
}
}).catch((error)=>{
alert("error buscar" + error);
});
}
function limpiar(){
lista.innerHTML="";
matricula="";
nombre="";
carrera="";
genero="masculino";
escribirInpust();
}
btnInsertar.addEventListener('click',insertDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnBorrar.addEventListener('click',borrar);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click', limpiar);

    /*
        El alumno realizará CRUD (Create, Read, Update, Delete) para la publicación dinámica de datos 
    */